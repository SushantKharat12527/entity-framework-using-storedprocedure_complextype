﻿using Sol_EF_SP_ComplexType.EF;
using Sol_EF_SP_ComplexType.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_SP_ComplexType
{
    public class UserDal
    {
        #region Declaration
        private UserdbEntities db = null;
        ObjectParameter status = null;
        ObjectParameter message = null;
        #endregion
        #region Constructor
        public UserDal()
        {
            db = new UserdbEntities();
        }
        #endregion
        #region Public method
        public async Task<IEnumerable<UserEntity>> GetUserData(UserEntity userEntityObj)
        {
            return await Task.Run(()=> {
                var GetQuery =
                db?.uspGetUser("Select"
               , userEntityObj?.UserId ?? 0
               , userEntityObj?.FirstName ?? null
               , userEntityObj?.LastName ?? null
               , userEntityObj?.UserLogin?.UserName ?? null
               , userEntityObj?.UserLogin?.Password ?? null
               , userEntityObj?.UserCommunication?.MobileNo ?? null
               , userEntityObj?.UserCommunication?.EmailId ?? null
               , status = new ObjectParameter("status", typeof(int))
               , message = new ObjectParameter("message", typeof(string))
                     )
                   ?.AsEnumerable()
                     ?.Select(this.SelectUser)
                     .ToList();
                return GetQuery;
            });
            
        }

        #endregion
        #region Private Property

        private Func<uspGetUserResultSet, UserEntity> SelectUser
        {
           get
            {
                return
                   (leUspGetUserResult) => new UserEntity()
                   {
                       UserId=leUspGetUserResult.UserId
                       ,FirstName = leUspGetUserResult.FirstName
                       ,LastName=leUspGetUserResult.LastName
                       ,UserCommunication=new UserCommunication()
                       {
                           MobileNo=leUspGetUserResult.MobileNo
                           ,EmailId=leUspGetUserResult.Email
                       }
                      ,UserLogin=new UserLoginEntity()
                      {
                          UserName=leUspGetUserResult.UserName
                          ,Password=leUspGetUserResult.Password
                      }
                       
                   };

            }
        }
        #endregion
    }
}
